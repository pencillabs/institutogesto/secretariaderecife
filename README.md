# Relatório da arquitetura da informação dos sistemas da secretaria de Educação do Estado de Recife.

## Quick Start

Baixe o gerenciador de dependencias Poetry:

	curl -sSL https://install.python-poetry.org | python3 -


Habilite o virtualenv do poetry:

	poetry shell

Instale as dependencias:

	poetry install

Compile o relatório:

	make build

Acesse o relatório em formato html:

	firefox build/index.html


## Gitlab Pages

Para publicar a página no gitlab pages é necessário subir no repositório o diretório build. Isso
é um alternativa para gerar as páginas estáticas sem precisar adicionar novos steps ao arquivo
`.gitlab-ci.yml`.
