=============================================
Consist - Sistema de Folha de Pagamentos
=============================================

Consist é o sistema de cadastro e folha de pagamento do Município há mais de vinte anos.
Utiliza Oracle como banco de dados e possui parte das funcionalidades implementadass em PHP, parte em ADS/Natural (linguagem própria da empresa Consist). A infraestrutura do sistema é mantida pela empresa municipal `Emprel <https://www.emprel.gov.br/>`_. O Consist possui uma arquitetura de *Mainframe*, por isso, é integrado a outros sistemas da Secretaria através de compartilhamento de arquivos. Apesar de ser um sistema antigo, os cálculos necessários para gerar as folhas de pagamentos são muito estáveis e atendem a complexidade do setor. O Consist é um sistema utilizado a nível municipal pelo Órgão de Planejamento, Gestão e Transformação Digital.

O Consist não utiliza um sistema de autenticação e controle de acesso próprio, diferente dos demais sistemas da Secretaria, que utilizam o EMAC ou ConectaLogin. O Município não possui acesso ao código-fonte do sistema, apenas à licença de uso. Existe um contrato de suporte e manutenção com a Consist e uma equipe interna que gera alguns relatórios a partir do que o sistema disponibiliza. Existe um setor no Órgão de Planejamento, Gestão e Transformação Digital que trata apenas da folha de pagamento da Secretaria de Educação, além de servidores da Secretaria que fazem a gestão de férias dos servidores.

Existe uma queixa do Órgão relacionado ao uso do Consist. Segundo o servidor que apresentou o sistema, a Secretaria de Educação utiliza um cadastro paralelo de gestão de pessoas. O que vai acontecer na prática (quando o SEGP for lançado) é que as bases de dados ficarão discrepantes. Como o sistema de Gestão de Pessoas da Secretaria não realiza pagamentos pode haver algum problema crítico no futuro por conta dessa não integração. Foi uma decisão da Secretaria não seguir com a integração com o Consist.

Principais funcionalidades
==============================

- **Gestão de pessoal**: permite cadastrar uma nova matrícula de um servidor na base de dados. Toda matrícula cadastrada deve seguir padrão de criação, informado manualmente por quem faz o cadastro.

- **Controle de frequência**: permite realizar o controle de frequência dos servidores. A Secretaria de Educação envia o registro de frequência e o Consist realiza os cálculos de desconto a partir disso. O processo é lento porque passa por vários outros setores internos até chegar no sistema. Isso gera problemas financeiros para o Município e para a Secretaria. **Não há uma integração entre o Consist e os sistemas da Secretaria para controle de frequência**.

- **Controle de afastamento**: permite afastar um servidor de suas atividades por diferentes motivos. O servidor precisa entrar com o pedimento de afastamento com bastante antecedência, já que o processo é lento e o pedido só é liberado quando o processo encerra.

- **Cadastro de folha**: permite realizar o cálculo da folha de pagamento do servidor. Ao longo do mês, as ocorrências (férias, admissão, dependentes, gratificações, frequência) são lançadas dentro de um calendário para a folha de pagamento.  Assim que o calendário encerra, faze-se os cálculos e é solicitado o pagamento para o Banco.  

O Consist interage com o `SOFIN <./sofin.html>`_  para realizar o fechamento da folha e fazer a transmissão dos dados.

