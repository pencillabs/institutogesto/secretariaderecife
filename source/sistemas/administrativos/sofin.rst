=====================================================
SOFIN - Sistema de Controle Orçamentário e Financeiro
=====================================================

SOFIN é o sistema de controle orçamentário e financeiro da Prefeitura utilizado pela Secretaria. 
Possui vários perfis de usuário e é gerenciado pela Secretaria de Finanças.  
O sistema utiliza banco de dados Oracle e parte dos módulos foi refeita em PHP. 
O restante dos módulos utiliza Centura, uma linguagem que não é mais mantida, dificultando a manutenção. 
A arquitetura do sistema é cliente-servidor.

O SOFIN registra e contabiliza todos os lançamentos do Município e do poder legislativo. Cada cálculo, processo ou tarefa executada no sistema está atrelada a alguma lei, por exemplo, a lei de responsabilidade fiscal. 

.. thumbnail:: ../../static/images/sofin/1.png


Principais módulos
===========================

- **Módulo Educação**: a equipe do SOFIN que atua na Secretaria precisa, ao final do ano, registrar no sistema a comprovação do uso de no mínimo 25% do orçamento anual disponibilizado. Essa meta, de 25%, vem da lei de responsabilidade fiscal. Infelizmente, por falta de uma cultura interna da Secretaria, muitas vezes esse registro é feito apenas no final do ano, gerando um trabalho muito grande para as equipes.
- **Módulo Recursos Humanos**: a folha de pagamento é importada no SOFIN (gerada no `Consist <./consist.html>`_) pela Secretaria de Finanças. O SOFIN formata a folha de pagamento para uma visão orçamentaria, contábil e financeira, exigida para apuração de contas.
- **Módulo Gestão de Contrato**: a Secretaria não possui um sistema de Gestão de Contrato e está avaliando utilizar o módulo do SOFIN para isso. Segundo o servidor Adeildo, algumas equipes da Secretaria precisam de um sistema de gestão de contratos (`SEINFRA <../pedagogicos/seinfra.html>`_), mas seguem fazendo o trabalho manualmente e registrando os contratos no portal da transparência.
- **Módulo Gestão de patrimônio**: módulo de integração do SOFIN com sistemas de patrimônio dos Órgãos. A Secretaria não possui um sistema para Gestão de Patrimônio.
- **Módulo Custos Públicos**: módulo do SOFIN para controle de despesas (compra de materiais) e custos públicos.

-------------------
Módulo Orçamento
-------------------

- **Cadastro de item**: cadastra materiais que irão ser adquiridos com o orçamento.
- **Solicitação de Empenho**: ao finalizar a inclusão de uma despesa, ela é convertida para uma nota de empenho (quando a despesa é executada). Uma solicitação pode ser cancelada e eliminada fisicamente do sistema. Uma vez convertida, uma despesa não pode mais ser removida, apenas anulada.
- **Liquidação de empenhos/subempenhos**: estágio de liquidação. É um estágio legal para o processo de pagamento. Dispara um gatilho para a Secretaria de Finanças.

------------------
Módulo financeiro
------------------

- **Programação financeira**: dá sutentação à execução do orçamento através do encaminhamento da GGAF (Gerência Financeira). Na Programação financeira é possível fazer as realocações entre sub-ação.
- **Saldo Financeiro**: é preciso ter o saldo orçamentário e o financeiro. A partir do saldo financeiro é que a solicitação de empenho é aprovada.
- **Movimentação Financeira / Ordem Provisão de Crédito**: uma vez liquidado no sistema de orçamentos, o empenho é registrado no sistema financeiro. A partir dai, a Secretaria de Finanças realiza o pagamento do empenho para uma conta da SEDUC. Em seguida, a SEDUC entra na função de **liberação Empenho**, para fazer o pagamento ao credor. Após a liberação, a Secretaria de Finanças gera um **Arquivo Envio** e faz o *upload* no sistema bancário. Os comprovantes de pagamento são enviados de volta para no módulo financeiro pela funcionalidade **Captura do Arquivo Retorno**, para comprovar os pagamentos.


Uso do SOFIN pela da Secretaria
================================

A Secretaria faz uso extensivo do módulo de orçamento do SOFIN. Todas as ações possíveis relacionadas ao orçamento da Secretaria são registradas neste módulo. A medida que a Educação vai utilizando os recursos, o saldo orçamentário no SOFIN vai sendo atualizado. Pela LOA (Lei de orçamento), a Secretaria já sabe o valor disponível para ser utilizado.
Uma vez com o orçamento disponível, a Secretaria registra as despesas que serão praticadas. O servidor da SEDUC realiza o cadastro da despesa e envia para a Secretaria de Finanças.

O orçamento é dividido em diferentes contas (rubrica). Essas contas podem exigir remanejamento por parte da Secretaria. Para algumas opções (sub-ação, elemento e sub-elemento) a Secretaria tem liberdade de remanejar, para outros é preciso fazer uma solicitação para os orgãos competentes.  O uso do SOFIN é feito por diferentes órgãos. A SEDUC tem acesso à parte do sistema, porém algumas necessidades precisam ser solicitadas, por exemplo, ao Conselho de Política Financeira.




O SOFIN foi apresentado pela servidora Andreia da Silva Morais.
