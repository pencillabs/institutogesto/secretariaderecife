=====================================
SEGM - Sistema de Gestão de Merendas
=====================================

O Sistema de Gestão de Merendas (SEGM) é um sistema desenvolvido para absorver demandas dos cadastros de pedidos de merenda escolar, feitos pelas unidades de ensino da rede. Até 2021, os pedidos eram feitos por comandas físicas e escaneadas para envio aos fornecedores. O sistema, que está em fase de implantação, vem para automatizar e facilitar este processo.

Perfis de usuário
==================

- **Gestor Escolar**: realiza o cadastro de solicitações de merenda. Faz operações referentes apenas ao contexto da unidade que administra.
- **Fornecedor**:  atende às solitações de merenda feitas pelos gestores.
- **Administrador do sistema**: tem acesso a todo o sistema.

Principais Funcionalidades
==========================

Gestor
------

- Cadastro de Cadum
- Cadastro de tipo de refeição
- Cadastro de programa
- Cadastro de aluno com dieta especial
- Cadastro de responsável por fornecedor
- Cadastro de ocorrências
- Solicitar pedido de refeição
- Acesso ao relatórios de faturamento e custos

Fornecedor
----------

- Atualização de situação do pedido
- Cadastro de ocorrências

Administrador do sistema
------------------------

- Bloqueio de fornecedores
- Importação de alunos via BATCH
- Definição de banner de cardápio

Requisitos e Arquitetura de Software
====================================

Utilizaremos duas técnicas para avaliarmos o SEGM. A primeira é a modelagem de casos de uso, que permite 
visualizar como atores interagem com as funcionalidades do sistema. O segundo é a modelagem
C4, que permite visualizar as fronteiras do sistema e com quais sistemas externos ele se comunica
para realizar suas operações.


Modelagem de casos de uso
--------------------------

O diagrama de casos de uso é uma forma visual de identificar perfis de usuário, funcionalidades do
sistema e a relação entre ambos. Iremos utilizar essa técnica de modelagem para termos uma visão
alto nível de como o SEGM funciona.

.. image:: ../../static/images/segm/4.png


Modelagem C4
--------------

Para entendermos as interações do SEGM com outros sistemas e bases de dados da
Secretaria, utilizaremos o modelo C4 para visualização de arquitetura de software. Para autenticar
no SEGM o usuário precisa ter suas credenciais criadas na base EMAC. O EMAC é um banco de dados
que gerencia as credenciais de acesso aos sistemas mais antigos da Secretaria. Novos sistemas estão
utilizando o Conecta Recife. Uma vez verificada as credenciais o usuário loga no sistema por meio de
sua interface *web*. A partir dai o usuário (gestor no caso) pode acessar as funcionalidades
apresentadas no modelo de casos de uso.


O SEGM lê e escreve de um banco de dados único chamado SECE. O SECE é responsável por
armazenar os dados de todos os sistemas da Secretaria. Para cada sistema, regras SQL são criadas
para limitar em quais tabelas é possível escrever ou recuperar dados.

.. image:: ../../static/images/segm/5.png


Referências
============

1. `What is Use Case Diagram <https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-use-case-diagram/>`_;
2. `C4 Modeling framework <https://c4model.com/>`_
