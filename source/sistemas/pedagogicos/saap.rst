=====================================================
SAAP - Sistema de Acompanhamento de Ações Pedagógicas
=====================================================

O Sistema de Acompanhamento de Ações Pedagógicas (SAAP) foi um sistema desenvolvido com intuito de acompanhar ações pedagógicas propostas pela Secretaria de Educação da prefeitura do Recife. Ações pedagógicas podem ser cursos de extensão ofertados para alunos, jovens, adultos e pessoas da comunidade. O sistema provê as funcionalidades necessárias para que uma ação pedagógica seja aplicada, desde o controle de alunos e enturmação, até o registro de diários e notas.

O sistema foi utilizado no ano de 2020 como alternativa ao SEMEWEB, devido a sua maior flexibilidade e a grande maioria de suas funcionalidades já cumprirem o mesmo papel.

Perfis de usuário
==================
- **Gestor**: responsável por criar ações pedagógicas em sua unidade de ensino.
- **Professor**: responsável por registrar diário de classe e atribuir notas aos alunos.
- **Administrador do sistema**: tem acesso a todo o sistema.

Principais Funcionalidades
==========================

Gestor
------

- **Criação de curso**: permite criar cursos ou ações pedagógicas na unidade de ensino em que é gestor.
- **enturmação**: permite enturmar alunos nos cursos criados, seguindo critérios de separação (comunidade, professores e alunos);
- **controle de avaliação**: permite gerenciar o formato de avaliação dos cursos, podendo definir notas mínimas e outros critérios.
- **Validação de registros**: permite validar registros enviados por professores para comprovação de entradas no diário de classe.
- **Cadastro de professores**: permite designar professores ou pessoas da comunidade para ministrar cursos.

Professor
---------

- **Registro no diário**: permite registrar no diário de classe a descrição da aula ministrada na data referida. Também faz *upload* de uma comprovação de que a aula foi realmente ministrada, seja em formato de vídeo ou documento.
- **Atribuição de notas**: permite atribuir notas aos alunos do curso, seja em valor, número ou formato de aprovação e reprovação para critérios mais abertos.
- **Consulta de turmas**: têm acesso aos alunos enturmados em seus cursos, assim como a presença nas aulas, caso seja critério exigido para conclusão do curso.

Administrador
-------------

- **Importação de dados**: permite importar dados de alunos através da exportação feita pelo SECE.
- Possui acesso às funcionalidades dos demais perfis para todas as unidades educacionais.
