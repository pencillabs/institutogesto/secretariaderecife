==================================
SDCL - Sistema de Diário de Classe
==================================

Sistema de Diário de Classe (SDCL) é um sofware desenvolvido na linguagem PHP que permite
realizar o acompanhamento das aulas ministradas e do histórico dos alunos ao longo do ano letivo.
A infraestrutura e desenvolvimento do sistema são feitos pela Emprel, empresa pública mantida pela Prefeitura de Recife. 


Perfis de usuário
==================

- **Professor titular da secretaria**: realiza acompanhamento dos estudantes nas turmas em que é titular. O SEMEWEB controla as condições para que um diário de classe seja relacionado a uma turma.
- **Professor substituto**: depois que o gestor realiza a enturmação, o professor substituto pode acessar apenas os diários das turmas em que ele foi enturmado. O login desse professor vem do EMAC. A diferença entre professor substituto e titular é feita pelo SDCL, não há diferenciação no EMAC entre ambos os perfis.
- **Direção da escola/gestor**: tem acesso de consulta em todo o sistema, porém só preenche o diário de classe se estiver substituindo um professor. Este perfil é que realiza o processo de substituição de professores. O perfil escola também vincula estudantes AEE a professores AEE. O registro do estudante AEE é feito no SEMEWEB.
- **Gestão da rede/gerência/equipe de TI**: utilizam o sistema para realizar consultas e acompanham o trabalho dos professores. Esse perfil tem acesso aos diários de classe.
- **Administrador**: tem acesso a todo o sistema, porém apenas para consulta.
- **AEE**: professor que acompanha os alunos especiais. O atendimento desse perfil é feito no contraturno. Nesse perfil, o registro do diário de classe é diferenciado do restante dos alunos não classificados como especiais.

Principais Funcionalidades
==========================

Professor
---------

- Cadastro do diário (cada sala é um diário) por professor.
- Registro das aulas (presenciais e aula-atividade).
- Recuperação automática do currículo definido pelo MEC.
- Controle da frequência dos alunos.
- Registro de reposição de aula.
- **Verificar pendências**: o sistema avisa que o professor não preencheu alguma informação do diário.
- **Verificar estudantes faltosos**: permite listar estudantes com mais de três faltas nos últimos 30 dias.
- Permite que o professor cadastre um projeto didático (não é muito utilizado).
- **Movimento e Rendimento Escolar**: permite que o professor faça uma consolidação do ano letivo a partir dos dados do diário.
- Cadastro de projeto didático;
- **Acompanhamento do estudante individualmente**: permite que o professor atualize informações de alunos específicos;
- **Registro de notas**: permite registrar as notas nas disciplinas por bimestre ou no ano letivo. Dependendo da média anual, o sistema abre recuperação (dependendo do número de reprovações, o estudante é reprovado diretamente, sem recuperação).
- **Relatório de parecer final do aluno**: permite gerar o relatório de resultado do aluno. O **SADR** possui uma funcionalidade parecida, mas que não é integrada ao Sistema de Diário de Classe.
- **Relatório de parecer final da turma**: permite gerar o relatório de resultado da turma. O **SADR** possui uma funcionalidade parecida, mas que não é integrada ao Sistema de Diário de Classe.
- **Pesquisa de estudantes**: permite pesquisar estudantes por nome, matrícula ou data de
  nascimento.
- **Relatório de curvas e farois**: gera o acompanhamento contínuo das turmas para verificar a relação entre frequência e notas.

Administrador
-------------
- Tem acesso a todas as funcionalidades do sistema, mas não pode alterar o diário de classe de um professor.
- Controle do ano letivo, para definir dias letivos e não letivos. Outros sistemas utilizam esse controle como, por exemplo, o Sistema de Merendas (`SEGM <sistemas/segm.html>`_).
- Gestão de carga horária por turma/escola;

Requisitos e Arquitetura de Software
====================================

O SDCL possui muitas funcionalidades que fogem do escopo de um diário de classe. 
Isso aconteceu porque a Secretaria não tinha um sistema de gestão de pessoas na época. O `SEGP <./segp.html>`_
está para ser lançado e irá permitir desabilitar algumas das funcionalidades do Diário de Classe
como, por exemplo, o controle de professores substitutos.

Utilizaremos duas técnicas para avaliarmos o SDCL. A primeira é a modelagem de casos de uso, que permite 
visualizar como atores interagem com as funcionalidades do sistema. O segundo é a modelagem
C4, que permite visualizar as fronteiras do sistema e com quais sistemas externos ele se comunica
para realizar suas operações.

Modelagem de casos de uso
--------------------------

O diagrama de casos de uso é uma forma visual de identificar perfis de usuário, funcionalidades do
sistema e a relação entre ambos. Iremos utilizar essa técnica de modelagem para termos uma visão
alto nível de como o SDCL funciona.

Nota-se que o perfil Professor é quem interage com a maioria das funcionalidades do sistema. 
Os demais perfis utilizam o sistema com foco na criação de relatórios e acompanhamento 
do empenho dos alunos e professores.

.. image:: ../../static/images/sdcl/1.png



Modelagem C4
--------------

Para entendermos as interações do SDCL com outros sistemas e bases de dados da
Secretaria, utilizaremos o modelo C4 para visualização de arquitetura de software. Para autenticar
no SDCL o usuário precisa ter suas credenciais criadas na base EMAC. O EMAC é um banco de dados
que gerencia as credenciais de acesso aos sistemas mais antigos da Secretaria. Novos sistemas estão
utilizando o Conecta Recife. Uma vez verificada as credenciais o usuário loga no sistema por meio de
sua interface *web*. A partir dai o usuário (professor no caso) pode acessar as funcionalidades
apresentadas no modelo de casos de uso.

O SDCL lê e escreve de um banco de dados único chamado SECE. O SECE é responsável por
armazenar os dados de todos os sistemas da Secretaria. Para cada sistema, regras SQL são criadas
para limitar em quais tabelas é possível escrever ou recuperar dados. Por ser o sistema responsável
pelo lançamento de notas e avaliaçãoes dos alunos, o SDCL deveria ser integrado ao `SADR <./sadr.html>`_, que também realiza avaliações das unidades educacionais. 
Essa integração não existe e é uma demanda colocada pelos professores da rede.

.. image:: ../../static/images/sdcl/2.png


Referências
============

1. `What is Use Case Diagram <https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-use-case-diagram/>`_;
2. `C4 Modeling framework <https://c4model.com/>`_
