===================================
SEGP - Sistema de Gestão de Pessoas
===================================

Sistema de Gestão de Pessoas (SEGP) é um sistem desenvolvido pela empresa `Stephanini <https://stefanini.com/pt-br>`_ para gestão dos funcionários (concursados ou não) da Secretaria. Permite registrar a atuação do servidor no Órgão. Quais cargos já ocupou, tipo de vínculo (sem vínculo, efetivo, estagio probatorio, contrato temporário) entre outras informações.


Perfis de usuário
=================

- **Servidor concursado (efetivo)**: visualiza apenas dos próprios dados.
- **Servidor tercerizado**: acessa algumas informações da gerência, mas não possui uma matrícula de servidor.
- **Administrador**: tem acesso à todas as telas e pode editar informações. É o perfil administrador quem define o perfil de acesso dos usuários.
- **Gerência**: servidores da estão em cargos de gestão. Tem uma visão menos focado nas escolas e mais focada nas unidades administrativas e nos servidores. Pode alterar dados e cadastrar novos servidores. Tem uma visão similar ao do administrador, mas não pode alterar os parâmetros do sistema.
- **Gestor**: gestor de uma unidade educacional específica. Tem acesso ao menu de Faltas, gestão do estágio probatório, afastamentos, férias entre outros. Esse perfil não pode alterar dados de cadastro dos servidores.

> Caso o usuário tenha mais de uma matrícula vinculado ao seu CPF e perfis de acesso configurados, o sistema solicita ao usuário a seleção de qual perfil ele quer utilizar.

Principais funcionalidades
===========================

- **Cadastro de usuário**:  permite cadastrar um novo usuário na base. Após o cadastro do usuário feito pelo perfil Gerência, o mesmo terá uma série de informações atreladas ao seu cadastro funcional. Este cadastro é pre-requisito para gerar uma nova matrícula para este servidor. A matrícula irá vincular ao usuário um conjunto de outras informações como cargo, função, categoria entre outros.
-  **Consulta de servidor**: permite buscar um servidor na base de dados. Um servidor pode ter mais de uma matrícula ativa, por isso, as consultas são feitas com o CPF.
-  **Manter Parâmetro**: permite cadastrar diferentes parâmetros no sistema. Esses parâmetros são utilizados como pré-requisitos para cadastrar uma nova matrícula. O cargo de um servidor, por exemplo, é um exemplo de parâmetro do sistema.
-  **Cadastro de matrícula**: permite, a partir do pré-cadastro, gerar uma matrícula para o servidor.  Com a matrícula criada, a gerência pode alocar o professor em alguma unidade educacional. Essa alocação pode ser definida como substituição ou para atividades escolares que não exija substituição.

Requisitos e Arquitetura de Software
====================================

Utilizaremos duas técnicas para avaliarmos o SEGP. A primeira é a modelagem de casos de uso, que permite 
visualizar como atores interagem com as funcionalidades do sistema. O segundo é a modelagem
C4, que permite visualizar as fronteiras do sistema e com quais sistemas externos ele se comunica
para realizar suas operações.

Diagrama de casos de uso
--------------------------

O diagrama de casos de uso é uma forma visual de identificar perfis de usuário, funcionalidades do
sistema e a relação entre ambos. Iremos utilizar essa técnica de modelagem para termos uma visão
alto nível de como o SEGP funciona.

.. image:: ../../static/images/segp/1.png

Modelagem C4
--------------

Para entendermos as interações do SEGP com outros sistemas e bases de dados da
Secretaria, utilizaremos o modelo C4 para visualização de Arquitetura de Software. Para autenticar
no SEGP o usuário precisa ter suas credenciais criadas na base ConectaRecife. Caso este usuário já tenha 
credenciais na base EMAC, automaticamente ele também terá no ConectaRecife. 
Uma vez verificada as credenciais, o usuário loga no sistema através de
sua *interface web*. Depois de logado, o usuário seleciona uma de suas matrículas ativas (se houver
mais de uma) e acessa as funcionalidades apresentadas na modelagem de casos de uso.

O SEGP lê e escreve de um banco de dados único chamado SECE. O SECE é responsável por
armazenar os dados de todos os sistemas da Secretaria. Para cada sistema, regras SQL são criadas
para limitar em quais tabelas é possível escrever ou recuperar dados

O SEGP gera dados consumidos pelo `SDCL <./sdcl.html>`_. Informações sobre os professores
substitutos são relevantes para o controle das turmas e aulas ministradas. Essa integração é feita
via SEME. O SEGP também permite alocar professores em determinadas unidades por
outras necessidades da rede.  Existe um detalhe relevante relacionado à relação entre SEGP e SDCL. No cadastro da matrícula, há uma opção de aula-atividade. Esse campo é específico para cargos de professor titular ou professor substituto, tendo relação com o cálculo de abono salarial. No SDCL também existe uma funcionalidade para registro de aula-atividade, mas ambos os sistemas não se conversam, o que pode gerar **retrabalho e duplicação da funcionalidade**.

.. image:: ../../static/images/segp/2.png
