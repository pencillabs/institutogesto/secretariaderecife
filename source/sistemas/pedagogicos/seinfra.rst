==========================================================
SEINFRA - Sistema de Gerenciamento da Capacidade Instalada
==========================================================

Sistema de Gerenciamento da Capacidade Instalada (SEINFRA) é um sistema desenvolvido para planejamento de matrícula e gestão da capacidade instalada das unidades de ensino da rede. O sistema tem acesso aos dados de progressão e retenção de estudantes e efetua os cálculos de vagas disponíveis para matrícula. Estes calculos precisam estar alinhados com as normativas do MEC de estudantes por metro quadrado e considerar a metragem das salas de aula aptas a receberem turmas. O SEINFRA atualiza os dados de vagas disponíveis e vagas liberadas no SECE.

É importante ressaltar que existe uma política de reserva de vagas que ficam indiponíveis para pré-matrícula. Isso ocorre porque é comum a Secretaria receber solicitações de matrícula pelo Ministério Público ou em algum outro caso de urgência.

Perfis de usuário
==================

- **Gestor Escolar**: o gestor escolar tem acesso aos recuros gerais do sistema. O perfil existe, mas ainda não foi liberado para as unidades devido a questões sensíveis de acesso, dado que um gestor de escola poderia bloquear vagas e favorecer algum estudante por interesse próprio.
- **Gerência**: perfil utilizado pela SIORE, com acesso completo ao sistema.

Principais Funcionalidades
==========================

Gestor escolar
--------------
- **Manter capacidade instalada da unidade**: permite cadastrar salas de aula, metragem e turnos disponíveis para indentificação de salas em condições de uso em cada período. O cadastro é feito apenas na unidade em que o gestor atua.
- **Planejamento de Vagas da unidade**: permite cadastrar vagas para cada unidade de ensino. O sistema consulta os dados de retenção e progressão dos estudantes de cada sala e, com base na normativa do MEC, calcula o número de vagas disponíveis que poderão ser liberadas para matrícula.

Gerência
--------
- **Manter capacidade instalada**: permite cadastrar salas de aula, metragem e turnos disponíveis para indentificação de salas em condições de uso em cada período. Possui acesso a todas as unidades da rede.
- **Planejamento de Vagas**: permite cadastrar vagas para cada unidade de ensino. O sistema consulta os dados de retenção e progressão dos estudantes de cada sala e, com base na normativa do MEC, calcula o número de vagas disponíveis que poderão ser liberadas para matrícula. Possui acesso a todas as unidades da rede.

Arquitetura da Informação
=========================

A arquitetura da informação do sistema é simples, dado que existem contextos bem definidos para a manutenção de capacidade instalada e para o planejamento de vagas. É importante ressaltar apenas que existe uma seleção de qual unidade está sendo operada. Essa seleção permite que diferentes perfis possam acessar apenas as unidades sob sua competência. Entretanto, os relatórios podem combinar dados de mais de uma unidade, ferindo a arquitetura do sistema, apesar de não ser um problema crítico.

.. image:: ../../static/images/seinfra/3.png

Requisitos e Arquitetura de Software
====================================

Utilizaremos duas técnicas para avaliarmos o SEINFRA. A primeira é a modelagem de casos de uso, que permite 
visualizar como atores interagem com as funcionalidades do sistema. O segundo é a modelagem
C4, que permite visualizar as fronteiras do sistema e com quais sistemas externos ele se comunica
para realizar suas operações.

Modelagem de casos de uso
-------------------------

O diagrama de casos de uso é uma forma visual de identificar perfis de usuário, funcionalidades do
sistema e a relação entre ambos. Iremos utilizar essa técnica de modelagem para termos uma visão
alto nível de como o SEINFRA funciona.

.. image:: ../../static/images/seinfra/1.png

Modelagem C4
--------------

Para entendermos as interações do SEINFRA com outros sistemas e bases de dados da
Secretaria, utilizaremos o modelo C4 para visualização de Arquitetura de Software. Para autenticar
no SEINFRA o usuário precisa ter suas credenciais criadas na base ConectaRecife. Caso este usuário já tenha 
credenciais na base EMAC, automaticamente ele também terá no ConectaRecife. 
Uma vez verificada as credenciais, o usuário loga no sistema através de
sua *interface web*. Depois de logado, o usuário seleciona uma de suas matrículas ativas (se houver
mais de uma) e acessa as funcionalidades apresentadas na modelagem de casos de uso.

.. image:: ../../static/images/seinfra/2.png

Autenticação e acesso
=====================
Todas as credenciais de acesso vem do sistema Emac.

Referências
============

1. `What is Use Case Diagram <https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-use-case-diagram/>`_;
2. `C4 Modeling framework <https://c4model.com/>`_
