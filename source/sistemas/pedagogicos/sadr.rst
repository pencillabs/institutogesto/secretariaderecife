=======================================
SADR - Sistema de avaliação diagnóstica
=======================================

O Sistema de Avaliação Diagnóstica (SADR) permite a construção de provas e avaliações a partir de uma estrutura de matriz curricular. Uma matriz é composta por diferentes eixos e cada eixo possui diferentes direitos de aprendizagem. Um direito de aprendizagem é um critério que será objeto de avaliação do professor em relação ao aluno. O SADR não utiliza uma estrutura de notas e sim de pesos. Cada peso representa o nível do aluno em relação à determinado direito de aprendizagem.

Perfis de usuário
==================

- **Professor**: só visualiza os resultados e insere os dados.
- **Administrador**: realiza a avaliação dos mapas da rede.
- **Escola**: só visualiza os resultados e insere os dados.

Principais funcionalidades
===========================

Administrador
-------------

- **Cadastro de matriz**: permite o cadastro de novas matrizes dentro do sistema. Essa
  funcionalidade é disponível apenas para administradores, diminuindo as possibilidades para o professor. Atualmente,
  existe uma única matriz para toda a rede.
- **Avaliação interna**: permite gerar um relatório da turma a partir do resultado individual dos
  direitos de aprendizagem, avaliados via avaliação (prova).
- **Avaliação externa**: permite gerar um relatório de uma unidade educacional a partir do resultado das turmas;
- **Cadastro de eixos**: permite cadastrar um novo eixo dentro de uma matriz.
- **Cadastro de direitos de aprendizagem**: permite cadastrar direitos de aprendizagem por ano
  escolar.

Professor
----------

**Incluir avaliação**: permitir criar uma prova que será aplicada para uma turma. 
Toda avaliação criada deve possuir um componente curricular. Uma avaliação é composta por um conjunto de questões. O professor cadastra no sistema a questão e o gabarito. O gabarito possui um nível de acertividade (pesos) que varia de 1 a 4.
**Mapa de consolidação**: permite gerar um relatório que apresenta o nível do estudante em relação a cada um dos direitos de aprendizagem. O mapa é um pdf estático, impedindo o professor de utilizar diferentes critérios para gerar o mapa.

Requisitos e Arquitetura de Software
====================================

Utilizaremos duas técnicas para avaliarmos o SADR. A primeira é a modelagem de casos de uso, que permite 
visualizar como atores interagem com as funcionalidades do sistema. O segundo é a modelagem
C4, que permite visualizar as fronteiras do sistema e com quais sistemas externos ele se comunica
para realizar suas operações.


Modelagem de casos de uso
--------------------------

.. image:: ../../static/images/sadr/1.png

Modelagem C4
--------------

O sistema não é integrado com o `SDCL <./sdcl.html>`_. Atualmente, os professores fazem avaliações que não seguem a estrutura da matriz definida no SADR, ou seja, a nota lançada nos diários não tem relação com a análise externa que os administradores fazem no SADR. Outro problema é que o Sistema de Diário de Classe não utiliza os direitos de aprendizagem nem os níveis de gabarito (pesos) do SADR. Se o professor quiser consolidar as notas utilizando a matriz do SADR, ele precisa fazer fora do sistema, utilizando planilhas.  O que os administradores e professores que utilizam o sistema necessitam é de uma integração entre SDCL e SADR. Precisa ser algo prático e automático para que o professor possa seguir uma jornada de uso que permita uma integração entre a matriz e o dia-a-dia em sala de aula. Seria importante também que houvesse uma forma de exportar as notas de um sistema para o outro.


.. image:: ../../static/images/sadr/2.png

