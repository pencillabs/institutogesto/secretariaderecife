========================================
GEQT - Sistema de Gestão de Equipamentos
========================================

Sistema de Gestão de Equipamentos (GEQT) é um software desenvolvido em Java Servlet cujo objetivo é
gerenciar o parque tecnológico (computadores, tablets, monitores) da Secretaria de Educação da prefeitura do Recife. 
Apesar de estar sendo utilizado com foco em equipamentos eletrônicos, o sistema poderia ser evoluido 
para atender outras demandas de almoxerifado.
O GEQT ainda não entrou em produção e o controle dos equipamentos segue sendo feito em planilhas. 
Apesar disso, a migração dos dados (uma média de dezesseis mil equipamentos) para o GEQT já está em andamento.
A plataforma foi desenvolvida pela empresa `Stephanini <https://stefanini.com/pt-br>`_.

Perfis de usuário
=================
- **Gestor geral**: tem acesso a tudo do sistema.
- **Gestor local**: tem acesso aos materiais de uma unidade específica.
- **Técnico**: executa ações específicas dentro da Secretaria, como configurar equipamentos em uma
  unidade.

Principais funcionalidades
==========================
 
Cadastro de Parâmetros
-------------------------------------------

- Permite cadastrar parâmetros que serão utilizados durante o cadastro de novos equipamentos. Alguns exemplos de parâmetros: fabricante do equipamento, modelo, tipo do equipamento/material, se é reastreavel ou não, entre outros.  Além dos parâmetros, é possível cadastrar a modalidade de aquisição. Se o modelo exige nota fiscal, no cadastro do equipamento o usuário tem que fazer o *upload* do documento.
- **Cadastro de Nota Fiscal**: é feito dependendo da modalidade do equipamento. Deve ser feito antes do cadastro do equipamento.

Cadastro de Equipamentos
-------------------------------------------

- **Cadastro de equipamento**: o equipamento é cadastro inicialmente no almoxerifado da Secretaria. No cadastro, vários parâmetros são vinculados ao equipamento como garantia, estado do equipamento (novo, quebrado), fabricante, modelo, quantidade. O cadastro dos parâmetros deve ser feito anteriormente. Durante o cadastro do equipamento, o usuário pode cadastrar quantos identificadores (número serial) ele quiser, por exemplo, em uma compra de 20 computadores de um mesmo modelo. Vale ressaltar que para o sistema, um equipamento é todo objeto que pode ser identificado por um número serial/identificador. Objeto que não tem número serial é cadastrado de forma quantitativa: cabos, conectores, entre outros.
- **Cadastro de espaço temporário**: permite cadastrar eventos que irão receber equipamentos emprestados pela Secretaria.

Cadastro de Almoxarifado
-------------------------------------------

- Permite incluir novos almoxerifados na base. Todo equipamento precisa estar vinculado a um almoxerifado.

Inventario
----------------
- **Vinculação de equipamento**: vincula um equipamento com uma escola, professor ou aluno. O equipamento pode ser rastreado (isso é feito pelo SELE). O equipamento possui diversos status como: entregue, pendência de termo.
- Permite visualizar ou consultar um equipamento.

Localização (SELE)
-------------------

É um subsistema dentro do GEQT. Ele tem uma arquitetura cliente-servidor em que o cliente é instalado no dispositivo. Quando o equipamento é ligado, o mesmo envia sua geolocalização para o servidor. Para que um equipamento seja rastreável, ele precisa ter um número serial.
- Permite acompanhar o histórico de localização do dispositivo.
- Permite o cadastro de alertas no sistema, para casos de furto, por exemplo.


Requisitos e Arquitetura de Software
====================================

Utilizaremos duas técnicas para avaliarmos o GEQT. A primeira é a modelagem de casos de uso, que permite 
visualizar como atores interagem com as funcionalidades do sistema. O segundo é a modelagem
C4, que permite visualizar as fronteiras do sistema e com quais sistemas externos ele se comunica
para realizar suas operações.

Modelagem de casos de uso
--------------------------

O diagrama de casos de uso consolida as principais funcionalidades identificadas na etapa
de apresentação do sistema. Nota-se que o perfil Gestor Geral é quem
executa a maioria das funcionalidades. Depois que o sistema for lançado e ficar estável, o acesso aos gestores locais será aberto.

.. image:: ../../static/images/geqt/1.png


Modelagem C4
-------------

Para entendermos as interações do SEMEWEB com outros sistemas e bases de dados da
Secretaria utilizaremos o modelo C4 para visualização de Arquitetura de Software. Para autenticar
no SEMEWEB o usuário precisa ter suas credenciais criadas na base EMAC. O Emac é um banco de dados
que gerencia todos os acessos aos sistemas mais antigos da Secretaria. Novos sistemas estão
utilizando o Conecta Recife. Uma vez verificada as credenciais, o usuário loga no sistema através de
sua interface *web*. A partir dai, o usuário (gestor geral) pode acessar as 
funcionalidades apresentadas no modelo de casos de uso.

O SEMEWEB lê e escreve de um banco de dados único chamado SECE. O SECE é responsável por
armazenar os dados de todos os sistemas da Secretaria. Para cada sistema, regras SQL são criadas
para limitar em quais tabelas é possível escrever ou recuperar dados. No caso do GEQT a
lista de escolas e alunos, por exemplo, são tabelas acessadas apenas no modo de leitura.

Além da gestão de equipamentos e parâmetros, o GEQT possui um subsistema chamado SELE (Sistema de
Localização de Equipamentos). Este sistema é responsável por geolocalizar os equipamentos utilizando
o sistema do Google como intermediário. Quando um equipamento é instalado por um técnico, o mesmo
envia sua geolocalização para o GEQT. Através do sistema de localização também é possível abrir
chamados para a situação de furto do equipamento.

.. image:: ../../static/images/geqt/2.png
