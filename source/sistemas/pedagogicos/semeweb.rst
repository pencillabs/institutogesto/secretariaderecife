=============================================
SEMEWEB/SEGE - Sistema de Gestão Escolar
=============================================

Sistema de Gestão Escolar (SEMEWEB) é um software *web* desenvolvido pela Emprel para gerenciar alunos, turmas e unidades de ensino.
Ele é o principal sistema da Secretaria e as informações geradas nele são utilizadas pelos demais
sistemas. O SEMEWEB é a evolução do SECE, um sistema antigo escrito na linguagem Delphi e que está para ser
descontínuado, apesar de ainda ser utilizado para, por exemplo, cadastrar novas unidades de
ensino (funcionalidade que não foi portada para o SEMEWEB). 

O SEMEWEB foi desenvolvido em PHP e será substituido por um novo sistema, o SEGE, desenvolvido em Java pela empresa `Stephanini <https://stefanini.com/pt-br>`_. O SEMEWEB ainda é o principal sistema utilizado pelos usuários, mesmo com o SEGE em estágio avançado de desenvolvimento. Isso ocorre por três motivos principais:

1. Nem todos os relatórios e funcionalidades foram migradas do SEMEWEB + SECE para o SEGE;
2. Existe uma resistência interna por parte das equipes de pararem de utilizar os sistemas mais
   antigos;
3. O time responsável pelo desenvolvimento do SEGE possui um volume de trabalho muito grande e ainda
   não conseguiu implementar os requisitos necessários para desligar os outros sistemas;

Pela relevância do SEMEWEB iremos mapear primeiro suas principais funcionalidades e perfis de usuário 
e depois falaremos do SEGE. A infraestrutura e desenvolvimento do sistema são feitos pela Emprel, empresa pública mantida pela Prefeitura de Recife. 


Perfis de usuário
==================

- **Gerencia de rede**: tem permissão de manipular dados de todas as unidades e alterar dados dos alunos.
- **Diretor**: tem permissão de consultar a base inteira, mas pode editar dados apenas dos alunos enturmados na sua escola.


Principais Funcionalidades
==========================

- **Matrícula para o estado**: permite gerar relatório de transferência de alunos do nono ano.
- **Ficha cadastral**: permite pesquisar um aluno por matrícula, nome, nome da mãe ou data de nascimento. Ao encontrar um aluno o usuário pode editar suas informações (documentações, endereço, histórico do aluno).
- **Desenturmação**: permite remover um aluno de uma escola para que ele seja transferido. O usuário pode definir a situação que motiva a desenturmação.
- **Grade curricular**: permite montar a grade curricular de cada um dos anos de ensino. Tanto **SDCL quanto SADR** utiliza os dados de grade do SEMEWEB.
- **Renomeação por ordem judicial**: permite renomear o nome do aluno.
- **Turmas cadastrados**: permite ver/criar/excluir turmas em uma unidade de ensino. Integra com o SDCL par saber se há alunos enturmados.
- **Lotação do docente**: lota um professor em uma escola específica.
- **Enturmação**: lota o professor em uma ou mais turmas. Essa funcionalidade foi implementada no `SEGP <./segp.html>`_ e será descontinuada do SEMEWEB/SEGE (quando o SEGP for lançado).
- **Cadastro de inscritos**: realiza uma inscrição do aluno. Quando o usuário confirma a inscrição, o sistema gera a matrícula. A partir da confirmação da inscrição os dados serão apresentados na **ficha cadastral**. O cadastro de inscrição não utiliza uma chave única, como o CPF, gerando diversas incosistências na base de alunos.
- **Transferência**: permite transferir um aluno entre unidades educacionais. O aluno precisa ser desenturmado antes.
- **Remaneja e reclassifica turmas**: transfere um aluno para outra turma na mesma unidade. Nessa opção não é preciso desenturmar o aluno.
- **Alocação**: para alunos que estejam pendentes de enturmação, permite alocar o aluno em uma turma;
- **Ajuste de turmas**: mesma lógica do remanejamento, mas não gera histórico. Muito utilizado no início do ano, quando as turmas ainda estão sendo organizadas.
- **Desfazer movimentação**: desfaz a movimentação do aluno entre turmas.
- **Ata de resultados**: utilizado na fase de senso do MAC. Permite constatar a situação de cada aluno de uma turma. Quando o Sistema de Diário de Classe não define a situação o usuário pode fazer a alteração com essa funcionalidade. Esse recurso existe tanto no SDCL quanto no SEMEWEB.
- **Cadastro de turma AEE**: permite cadastrar turmas para alunos especiais.
- **Relatórios**: permite gerar diferentes relatórios. Relatórios de matrícula, ficha do aluno, ata de resultado, alunos com deficiência, alunos com matrícula duplicada entre outros. Os relatórios são engessados e não permitem aplicar filtros para customização do resultado. O relatório de matrícula duplicada demonstra que a base está com erros de integridade porque existem alunos que possuem mais de uma matrícula. Tanto Matrícula Online quanto o cadastro de inscritos (matrícula balcão) não utilizam uma chave única (como CPF) para identificar se o aluno já existe na base.
- **Certificação histórico**: permite inserir os dados do aluno, observações, disciplinas e as notas. Essa funcionalidade não tem integração com o Sistema de Diário de Classe. Depois do preenchimento o usuário emite o certificado do usuário. Pela falta de integração com o SDCL esse relatório é emitido **manualmente**.
- **Exclusão de aluno**: permite mover os dados de uma matrícula para outra. O usuário informa uma matrícula para excluir e uma matrícula para mover o historico do aluno.

Requisitos e Arquitetura de Software
====================================

Utilizaremos duas técnicas para avaliarmos o SEMEWEB. A primeira é a modelagem de casos de uso, que permite 
visualizar como atores interagem com as funcionalidades do sistema. O segundo é a modelagem
C4, que permite visualizar as fronteiras do sistema e com quais sistemas externos ele se comunica
para realizar suas operações.

Modelagem de casos de uso
--------------------------

O diagrama de casos de uso é uma forma visual de identificar perfis de usuário, funcionalidades do
sistema e a relação entre ambos. Iremos utilizar essa técnica de modelagem para termos uma visão
alto nível de como o SEMEWEB funciona.

Pela modelagem, identificamos que algumas funcionalidades do SEMEWEB estão implementadas em outros
sistemas como, por exemplo, a funcionalidade de enturmar professor, que também foi desenvolvida no
SEGP. Uma funcionalidade que existe no SEMEWEB e no Sistema de Diário de Classe é a de dar notas para determinado aluno. No caso do SEMEWEB, uma espécie de histórico do aluno é gerado, mas pelo que foi relatado essa funcionalidade está desatualizada no sistema. Não se sabe atualmente como os professores tem gerado esse histórico do aluno, se pelo SEMEWEB, pelo Diário de Classe ou manualmente.

.. image:: ../../static/images/semeweb/1.png

Modelagem C4
--------------
Para entendermos as interações do SEMEWEB com outros sistemas e bases de dados da
Secretaria utilizaremos o modelo C4 para visualização de Arquitetura de Software. Para autenticar
no SEMEWEB o usuário precisa ter suas credenciais criadas na base EMAC. O Emac é um banco de dados
que gerencia todos os acessos aos sistemas mais antigos da Secretaria. Novos sistemas estão
utilizando o Conecta Recife. Uma vez verificada as credenciais, o usuário loga no sistema através de
sua interface *web*. A partir dai, o usuário (gestor da rede ou diretor) pode acessar as funcionalidades apresentadas no modelo de casos de uso.

O SEMEWEB lê e escreve de um banco de dados único chamado SECE. O SECE é responsável por
armazenar os dados de todos os sistemas da Secretaria. Para cada sistema, regras SQL são criadas
para limitar em quais tabelas é possível escrever ou recuperar dados. Por ser responsável
pela gestão de alunos, turmas e unidades educacionais praticamente todos os sistemas lêem das
tabelas do SEMEWEB. Para simplificarmos o entendimento, apresentamos no diagrama apenas Diário de Classe e
Matrícula Online consumindo tais tabelas.

.. image:: ../../static/images/semeweb/2.png


SEGE
=====

O SEGE é o sistema que irá substituir o SEMEWEB. Está em desenvolvimento contínuo, buscando
implementar as principais funcionalidades e relatórios utilizados pelas equipes de gestão escolar, 
além de desenvolver novas funcionalidades que não existem nos sistemas mais antigos. 
O novo sistema possui uma identidade visual mais moderna e intuitiva, mas
enfrenta resistência por parte dos usuários para ser adotado. Isso ocorre principalmente porque nem
todas as funcionalidaees do SEMEWEB existem no SEGE. A equipe do SEGE além de
trabalhar na implementação das funcionalidades do SEMEWEB também está portando funcionalidades do
SEME como, por exemplo, o cadastro de unidades escolares. Os Perfis de usuário a princípio serão os
mesmos do SEMEWEB.

Principais Funcionalidades
==========================

- **Cadastro de unidade de ensino**: permite cadastrar uma nova unidade. **O Semeweb não tem essa funcionalidade** e atualmente ela é feita no SECE.
- **Censo Escolar**: permite gerar o censo anual exigido pelo MAC.
- **Tabelas**: funcionalidade migrada do SECE e que não existe no SEMEWEB.
- **Cadastro de ano de ensino por unidade**: permite recuperar/cadastrar os anos de ensino em cada unidade educacional.
- **Cadastro de profissional**: recupera dados dos professores/gestores gerados no **SEGP**;
- **Calendário de matrícula**: permite estruturar o processo de matrículas em etapas. O **Matrícula Online** utilza essas informações do SEGE. Quando a matrícula online é liberada, a funcionalidade matrícula-balcão é bloqueada. Depois que o período de matrícula online encerra, tanto matrícula-balcão quanto outras funcionalidades são liberadas para as escolas.
- **Cadastro de primeiro dia letivo**: funcionalidade importante para definir quando o SDCL pode iniciar o registro de frequência. O SDCL possui uma funcionalidade de calendário letivo.
- **Tabelas/modalidade de ensino**: permite criar uma modalidade de ensino.
- **Tabelas/Disciplinas**: permite visualizar todas as disciplinas ofertadas pela rede. Funcionalidade migrada do SECE.
- **Tabelas/Grade Curricular**: vincula disciplinas com modalidades de ensino.
- **Tabelas/Bilingue libras**: identifica qual ano de ensino terá a disciplina de libras.
- **Perfil de acesso**: permite controlar quais funcionalidades cada perfil vai ter acesso. Todo o controle é feito no EMAC e o SEGE consome essas informações.
- **Google**: integra o SEGE com o google classroom.
- Todas as funcionalidades de gestão dos alunos já foram migradas do semeweb.


Referências
============

1. `What is Use Case Diagram <https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-use-case-diagram/>`_;
2. `C4 Modeling framework <https://c4model.com/>`_
