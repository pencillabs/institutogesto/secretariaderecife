================
Matrícula Online
================

O Sistema de Matrícula Online é um software que permite realizar operações de pré-matrícula (reserva). Essas operações são executadas pelo responsável legal, sem a necessidade de ir presencialmente na unidade de ensino para entrega de documentação e cadastro. A confirmação da matrícula é presencial, executada no SEMEWEB pelo servidor presente na unidade.

Perfis de usuário
==================

- **Responsável legal**: realiza os preenchimentos dos formulários de reserva de matrícula. Após o envio, o responsável ainda precisa confirmar a matrícula presencialmente na escola, essa confirmação é feita no SEMEWEB. O SEMEWEB controla as condições para preenchimento das reservas com base nas vagas liberadas pelo `SEINFRA <./seinfra.html>`_.

Principais Funcionalidades
==========================

Responsável Legal
-----------------

- **Cadastro da pré-matrícula em 4 etapas**: dados do responsável, dados do estudante, ano e Unidade de ensino e confirmação dos dados.
- **Consulta de escolas por geolocalização**: possibilita encontrar unidades através de filtros, organizadas no mapa de Recife.
- **Entrar em contato pela plataforma**: permite preenchimento de um formulário de contato.
- **Consulta do calendário de matrícula**: permite consultar Informações do calendário de matrícula.
- **Consulta de pré-matrícula**: consulta através de informações do estudante ou número do protocolo.

Arquitetura da Informação
=========================

O diagrama apresenta uma versão da arquitetura da infrmação da aplicação.

.. image:: ../../static/images/matricula/3.png

Requisitos e Arquitetura de Software
====================================

A infraestrutura e manutenção do sistema é mantida pela empresa Emprel, empresa pública mantida pela Prefeiturade Recife.  Utilizaremos duas técnicas para avaliarmos o Matrícula Online. A primeira é a modelagem de casos de uso, que permite 
visualizar como atores interagem com as funcionalidades do sistema. O segundo é a modelagem
C4, que permite visualizar as fronteiras do sistema e com quais sistemas externos ele se comunica
para realizar suas operações.

Modelagem de casos de uso
=========================

O diagrama de casos de uso é uma forma visual de identificar perfis de usuário, funcionalidades do
sistema e a relação entre ambos. Iremos utilizar essa técnica de modelagem para termos uma visão
alto nível de como o Matrícula Online funciona.

Nota-se que não existe correlação entre as funcionalidades de "Buscar escolas por localização" e "Cadastrar uma pré-matrícula", o que exige que o usuário execute um passo a mais no fluxo de matrícula. Uma sugestão seria que a partir da busca por unidade, o usuário já pudesse selecionar uma unidade para efetuar a matrícula.

`Link para modelagem de casos de uso do sistema
<https://drive.google.com/file/d/1m0dKX8GkOnwBUUhrjB2xNTrKGe7yGVfP/view?usp=sharing>`_

.. image:: ../../static/images/matricula/2.png

.. image:: ../../static/images/matricula/1.png


Autenticação e acesso
=====================
As funcionalidades para matrícula online não dependem de autenticação.

Referências
============

1. `What is Use Case Diagram <https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-use-case-diagram/>`_;
2. `C4 Modeling framework <https://c4model.com/>`_
