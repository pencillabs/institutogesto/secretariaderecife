==========
Conclusão
==========

A construção deste relatório é resultado das diversas reuniões feitas entre os consultores da Pencillabs e os times técnico e de gestão da Secretaria de Educação da Prefeitura do Recife. 
Estas reuniões, feitas de modo remoto, nos permitiram coletar diversas informações sobre os sistemas, suas particularidades e principais problemas enfrentados pelos usuários. Foram analisados doze sistemas ao
todo, sendo dez sistemas pedagógicos e dois sistemas administrativos e financeiros. Os sistemas pedagógicos analisados foram: `Sistema de gestão de pessoas (GEQT) <sistemas/pedagogicos/geqt.html>`_, `Sistema de Acompanhamento de Ações Pedagógicas (SAAP) <sistemas/pedagogicos/saap.html>`_, `Sistema de Diário de Classe (SDCL) <sistemas/pedagogicos/sdcl.html>`_, `Sistema de gerenciamento da capacidade instalada (SEINFRA) <sistemas/pedagogicos/seinfra.html>`_, `Sistema de avaliação diagnóstica (SADR) <sistemas/pedagogicos/sadr.html>`_, `Sistema de Gestão de Pessoas (SEGP) <sistemas/pedagogicos/segp.html>`_, `Sistema de Gestão de Equipamentos (GEQT) <sistemas/pedagogicos/geqt.html>`_, `Semeweb/SEGE <sistemas/pedagogicos/semeweb.html>`_ e `Sistema de Gestão de Merendas (SEGM) <sistemas/pedagogicos/segm.html>`_. Os sistemas administrativos e financeiros analisados foram: `Consist <sistemas/administrativos/consist.html>`_ e `SOFIN <sistemas/administrativos/sofin.rst>`_.

Para cada sistema foi feito o levantamento das principais funcionalidades, perfis de usuário e comunicação com outros sistemas e bases de dados.
O trabalho de modelagem e análise somados à comunicação frequente com os times da Secretaria, nos permitiram ter uma visão mais acertiva dos principais problemas tecnológicos e de governança enfrentados pelos servidores do Órgão.

Nesta etapa do relatório iremos apresentar nossas conclusões focando em três temas: Arquitetura de
Software, Arquitetura da Informação e necessidades dos usuários. Apresentaremos um diagrama que
representa a fotografia dos sistemas que foram objeto de análise do relatório. Discutiremos os
principais problemas encontrados e apresentaremos um plano de ação que a Secretaria poderá utilizar
como suporte para tomada de decisão.

Diagrama geral
====================

.. thumbnail:: static/images/conclusão/1.png

**obs: Para uma melhor visualização, clique com o botão direito na imagem e abra a mesma em uma nova
aba.**

Lista de sistemas do diagrama geral
------------------------------------


.. list-table:: Lista de sistemas avaliados
   :widths: 20 20
   :header-rows: 1

   * - Nome
     - Análise no relatório
   * - SEMEWEB e SEGE - Sistema de Cadastro Escolar
     - `SEMEWEB e SEGE <sistemas/pedagogicos/semeweb.html>`_
   * - SDCL - Sistema Diário de Classe On-line
     - `SDCL <sistemas/pedagogicos/sdcl.html>`_
   * - SAAP - Sistema de Acompanhamento de Ações Pedagógicas
     - `SAAP <sistemas/pedagogicos/saap.html>`_
   * - SEGM - Sistema de Alimentação Escolar
     - `SEGM <sistemas/pedagogicos/segm.html>`_
   * - SEGP - Sistema de Gestão de Pessoas
     - `SEGP <sistemas/pedagogicos/segp.html>`_
   * - Matrícula Online
     - `Matrícula Online <sistemas/pedagogicos/matricula.html>`_
   * - SEINFRA - Planejamento de vagas e capacidade instalada
     - `SEINFRA <sistemas/pedagogicos/seinfra.html>`_
   * - SADR - Sistema de Avaliação diagnóstica da Rede
     - `SADR <sistemas/pedagogicos/sadr.html>`_
   * - GEQT - Sistema de Gestão de Equipamentos
     - `GEQT <sistemas/pedagogicos/geqt.html>`_


Conclusão das análises
======================

Arquitetura de Software
------------------------

Como é possível perceber no diagrama geral, todos os sistemas da secretaria consomem e escrevem em
um único banco de dados Oracle chamada SECE. Cada sistema possui regras que limitam quais tabelas podem ser acessadas no modo leitura e quais podem ser acessadas no modo de escrita. 
O controle de restrição de acesso é todo feito no SECE, através de `DataSource <https://docs.oracle.com/cd/B14099_19/web.1012/b14012/datasrc.htm#CIHDHCCC>`_. 
Todos os sistemas, bancos de dados e demais unidades de software são mantidos pela empresa Emprel.

Durante o processo de levantamento de dados e modelagem notamos alguns momentos de instabilidade nos sistemas da Secretaria. Relatórios que não carregavam, informações que não eram apresentadas corretamente entre outros problemas de instabilidade e responsividade. Após ouvir tanto servidores da camada de gestão da Secretaria, professores e a própria Emprel, concluímos que não é possível afirmar que tal instabilidade se dá **apenas** pelo fato de todos os sistemas conectarem em uma base de dados única. 
Para investigar esses problemas e chegar a uma conclusão correta, seria necessária uma consultoria com acesso total ao código-fonte dos sistemas e a infraestrutura de servidores. Esta consultoria não teve acesso à nenhum código, máquina virtual, sistema ou unidade de software que permitisse tais avaliações. Vale ressaltar que muitos dos sistemas avaliados utilizam tecnologias antigas e legadas (`SDCL <sistemas/pedagogicos/sdcl.html>`_, `SADR <sistemas/pedagogicos/sadr.html>`_), contribuindo para lentidões e funcionamentos incorretos.

Por não ser escopo desse relatório avaliar o desempenho dos sistemas da Secretaria, iremos nos ater
ao que foi possível coletar junto à Emprel e com outros atores envolvidos com desenvolvimento e
infraestrutura no Órgão. O processo de *deploy* das aplicações está automatizado com
`Jenkins <https://www.jenkins.io/>`_, o código das aplicações é versionado no `Gitlab <https://gitlab.com/>`_ e existe uma política interna de atualização dos ambientes em produção. Existe um movimento interno na Emprel de começar a expor recursos dos sistemas via API, principalmente para atender projetos *mobile*, como o aplicativo do Sistema de Diário de Classe que está sendo desenvolvido.

Arquitetura da informação
-------------------------

Dos dez sistemas analisados no relatório, apenas GEQT e SEGE possuem uma boa identidade
visual, além de uma estrutura de navegação que auxilie o usuário na execução das tarefas. O restante
não é *mobile-friendly* e mesmo no *desktop* ou monitores maiores apresentam problemas
de responsividade. Além dos problemas de *interface* também temos problemas na jornada do usuário,
exigindo vários *clicks* para execução de tarefas simples.

Um ponto de atenção relacionado a arquitetura da informação mas também relacionado à governança é a dificuldade que a Secretaria tem de "encaixar" seus sistemas dentro de processos organizacionais. 
Em diversos momentos das reuniões foram citados problemas que ocorrem no ambito digital, mas que surgem no ambito humano e de organização do trabalho. 
É vital que a Secretaria faça um trabalho interno que mapeie não só funcionalidades, perfis de
usuário e arquitetura da informação (foco deste relatório), mas também processos e procedimentos envolvendo as relações entre setores, o calendário letivo e até mesmo outros orgãos como o próprio MEC. A decisão de alterar ou desenvolver novos sistemas deve estar alinhada com processos organizacionais bem definidos.


Necessidades dos usuários
--------------------------
Os sistemas da Secretaria possuem diferentes perfis de usuário com diferentes níveis de acesso. 
Concluímos que os dois principais perfis são **Gestor** e **Professor**.

---------
Gestor
---------

A principal necessidade do perfil Gestor é conseguir gerar inteligência de
negócio a partir dos dados gerados pelos diferentes sistemas do órgão. Atualmente, esse
perfil não possui nenhum ponto de acesso ao SECE. Muitas vezes, é preciso
solicitar que servidores com acesso ao banco de dados extraiam informações relevantes para a camada
de gestão, tornando o processo demorado e burocrático.

Conseguir extrair informações acessíveis e confiáveis do SECE sem ser necessário entrar em cada um dos sistemas, resolveria os principais problemas dos gestores da Secretaria. Sabemos que cada sistema possui um login próprio (mesmo que integrado no EMAC ou ConectaLogin) e que problemas técnicos de autenticação dificultam o acesso à informação e a tomada de decisão. O time de gestão precisa de um ambiente intuitivo, acessível e que permita recuperar informações do SECE sem ter que depender de terceiros e sem ter que entrar em cada um dos sistemas individualmente.

Houve um projeto envolvendo tecnologias de Inteligência de Negócio que utilizaram
o sistema `ClickView <https://www.qlik.com/us/products/qlikview>`_. Segundo um analista da Emprel, alguns `dashboards <http://qlikview.recife.pe.gov.br/>`_ foram construídos para que as equipes de gestão pudessem ter acesso a diferentes dados presentes no SECE. Não sabemos se seria possível reutilizar esse projeto, mas parte da solução deve: identificar quais são essas informações relevantes para os gestores e qual a melhor forma de visualizá-las.

---------
Professor
---------

A Secretaria possui sistemas que resolvem problemas de diferentes contextos. `GEQT <sistemas/pedagogicos/geqt.html>`_, por
exemplo, é utilizado para a gestão do parque tecnológico. Temos também o `SEGM
<sistemas/pedagogicos/segm.html>`_, voltado para gestão de merendas. Boa parte dos sistemas avaliados por esse
relatório são secundários à atividade docente. Secundários no sentido de que não tem relação direta
com o trabalho de sala de aula. Os principais sistemas voltados para o professor que
analisamos foram `SDCL <sistemas/pedagogicos/sdc.html>`_ e `SADR <sistemas/pedagogicos/sadr.html>`_.

O Sistema de Diário de Classe (SDCL) é o principal sistema para o professor realizar o acompanhamento dos seus alunos e turmas.  Suas funcionalidades são indispensáveis para determinar, por exemplo, se o aluno passou ou reprovou
de ano. O SADR atua mais voltado na definição metodológica do ensino, estruturando a matriz, eixos e
direitos de aprendizado. Juntos, deveriam entregar ao professor ferramentas tanto para o
dia-a-dia da sala de aula quanto para uma visão geral do ensino aplicado pela Secretaria.

Os principais problemas que identificamos com estes sistemas foram os seguintes:

1. Não integração entre a avaliação do professor (feita via SDCL) e as diretrizes definidas no SADR;
2. Ausência da possibilidade do professor realizar uma avaliação interna (na turma) utilizando a
   matriz/eixo/direitos de aprendizado definidos no SADR. Hoje esse processo é feito manualmente;
3. Ausência da possibilidade de avaliar um aluno em específico utilizando a matriz/eixo/direitos de aprendizado definidos no SADR;

Além disso, os usuários do SDCL tem reportado constantemente problemas de acesso ao sistema. Em um
relatório de chamados disponibilizado por servidores da Secretaria, dos 8975 chamados abertos, 2354 tinham
relação com o sistema Diário de Classe. Avaliando estes 2354 chamados, 1083 (quase metade) são relacionados à problemas de perfil de acesso e autenticação (login e senha).
O diagrama a seguir apresenta o número de chamados por sistema: 

.. image:: static/images/conclusão/3.png

A planilha com os dados dos chamados pode ser encontrada `aqui <static/data/conclusão/chamados.xlsx>`_.


Plano de Ação
=============

A partir das conclusões apresentadas, iremos propor um plano de ação para que a
Secretaria dê os primeiros passos na correção dos problemas identificados. Entendemos que
existem diferentes perfis envolvidos na atividade de desenvolvimento, manutenção, uso e gestão dos
sistemas. Por isso é necessário pensarmos possibilidades que se adequem a disponibilidade de
recursos humanos e tecnológicos da Secretaria. Iremos dividir o Roadmap em produtos para facilitar a
priorização.

Produto 1
----------

O produto 1 será a construção de um sistema de inteligência de negócio voltado para as equipes de
gestão da Secretaria. Esse sistema terá como escopo principal permitir que o usuário **visualize e recupere** informações do SECE. Conforme o levantamento que fizemos para o relatório, essa é a principal necessidade do perfil **Gestor** na Secretaria de Educação.

Essa nova plataforma deve permitir ao gestor buscar informações em toda a base de dados e, a partir
disso, construir ou reutilizar diferentes formas de visualização. 
Sugerimos para esse produto o uso do conjunto de ferramentas do projeto
`Elastic Stack <https://www.elastic.co/pt/>`_. 
O Elastic Stack é uma plataforma de **código aberto** voltada para busca e visualização de dados. Com ela, é possível extrair dados de diferentes fontes e, a partir disso, construir dashboards dinâmicos e relatórios estruturados. O Elastic Stack é utilizado por empresas como Uber e Netflix, possui uma vasta documentação e seus recursos estão disponíveis para qualquer organização ou empresa.

Para ser possível buscar em todas as tabelas do SECE um projeto de desenvolvimento será necessário.
O Elastic Stack possui um modelo próprio de armazenamento dos dados que não é diretamente
compatível com o SECE, ou seja, não é possível fazer as buscas direto no banco Oracle. Por isso,
será necessário um projeto que importe os dados do SECE para a base de dados do ElasticSearch. Com
os dados importados seremos capazes de indexa-los para que buscas textuais em toda a base sejam
possíveis. Para visualizar o resultado das buscas e construir dashboards dinâmicos, utilizaremos o
Kibana, ferramenta do Elastic Stack voltada para a visualização dos dados. O diagrama a seguir
apresenta uma visão geral do projeto.

.. thumbnail:: static/images/conclusão/2.png

O objetivo final é termos uma plataforma de visualização de dados que tenha acesso de leitura
ao banco de dados Oracle. Dados novos inseridos são periodicamente importados pelo serviço `LogStash <https://www.elastic.co/guide/en/logstash/current/plugins-inputs-jdbc.html>`_. Os dados importados são armazenados no ElasticSearch, que utiliza um modelo de dados não-relacional para otimizar buscas textuais. O Kibana fica responsável pela criação e visualização dos dashboards. Com essa estrutura, um gestor poderia criar um Dashboard focado em dados dos professores enquanto outro poderia focar na capacidade instalada da Secretaria.

Produto 2
----------

O produto 2 será a construção de uma plataforma de controle de autenticação e permissão para os demais sistemas da Secretaria. Dados extraidos de `uma planilha de chamados <static/data/conclusão/chamados.xlsx>`_ demonstram que a maioria das dificuldades dos usuários têm relação com autenticação, sendo o Diário de Classe o sistema com maior número de chamados abertos. Nosso relatório concluiu que o fato de cada sistema ter um login próprio (seja via EMAC ou ConectaLogin) dificulta o acesso e integração entre os sistemas. O objetivo é permitir que com um único acesso o usuário esteja automaticamente autenticado em todos os sistemas que estiverem integrados ao produto 2.


O primeiro passo deve ser avaliar se a partir do ConectaLogin seria possível
autenticar um mesmo usuário em diferentes sistemas da Secretaria. Além da autenticação, essa
avaliação também deve investigar a possibilidade de controle de permissão por meio do login único.
Avaliamos que esse é um primeiro passo viável, já que o ConectaLogin utiliza internamente o projeto `Keycloak <https://www.keycloak.org/>`_. 
Keycloak é um projeto de código aberto, voltado para autenticação e controle de acesso e que possui um
serviço de *Single-Sign on*. Propomos que o trabalho comece pelo `SEGP <./sistemas/segp.html>`_ e
pelo `SEGE <./sistemas/semeweb.html>`_, que já utilizam ConectaLogin. Logo que os primeiros passos
em direção ao login único forem dados é extremamente importante avaliar o esforço para adaptar
outros sistemas, como Diário de Classe, ao ConectaLogin. Sem isso, o produto 2 irá conseguir
intermediar a autenticação de poucos sistemas, podendo não resolver o problema da Secretaria
completamente. O produto 2 também ficará responsável por integrar o Produto 1 à estrutura de login
único. O diagrama a seguir apresenta uma visão geral da solução:

.. thumbnail:: static/images/conclusão/4.png



Este relatório foi produzido pela empresa `Pencillabs <https://pencillabs.com.br/>`_. Para duvidas
ou sugestões, entre em contato por `contato@pencillabs.com.br`.
