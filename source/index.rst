Análise técnica dos sistemas da Secretaria de Educação da Prefeitura do Recife.
===============================================================================

A Secretaria de Educação da Prefeitura do Recife possui diversos sistemas para suprir diferentes
demandas internas. A `Emprel <https://www.emprel.gov.br/>`_, empresa municipal que presta serviço
para o Órgão, é a responsável pela manutenção e infraestrutura desses sistemas.
Para dar vasão a diferentes demandas internas, a empresa `Stephanini <https://stefanini.com/pt-br>`_ 
foi contratada para atuar como Fábrica de Software e desenvolver novos projetos para a Secretaria. 
Como atua apenas no desenvolvimento, existe uma relação com a Emprel para disponibilizar o que é
desenvolvido nos ambientes de produção e homologação. Atualmente, existem sistemas voltados para a
atividade docente, controle do parque tecnológico, gestão de pessoas, avaliação pedagógica entre
outros.

A camada de gestão da Secretaria, que precisa atender tanto aos usuários quanto as
esferas superiores, não possui uma visão geral e estratégica 
de quais funcionalidades cada sistema possui, quem são os principais atores envolvidos no
uso e como os sistemas estão interligados. Além dos problemas técnicos, 
a Secretaria não possui nenhuma documentação que consolide informações
básicas de escopo como: principais funcionalidades, perfis de usuário, comunicação entre os
sistemas (interoperabilidade) e armazenamento dos dados. Para entender o que um sistema faz é
necessário consultar servidores da rede que conheçam o sistema a fundo.

É nesse contexto que este relatório foi produzido. Nosso principal objetivo é munir a camada
de gestão com informações técnicas sobre os sistemas e suas conecções. Com essas informações, os
servidores  conseguirão entender como e por quem os sistemas são utilizados, as principais
dificuldades dos usuários, funcionalidades repetidas ou obsoletas, aonde os dados são armazendos e,
principalmente, poderão ter uma visão de como os sistemas funcionam em conjunto.

Sistemas
---------

Os sistemas que serão objeto de análise são mantidos e hospedados pela Emprel. Este
relatório será construído a partir de entrevistas e questionários feitos com os servidores
escolhidos para atuarem como facilitadores. Não teremos acesso ao código fonte de nenhum sistema,
muito menos a infraestrutura ou máquinas virtuais em que os sistemas são disponibilizados. O
diagrama a seguir, apresenta como será nosso processo de trabalho:

.. thumbnail:: static/images/introdução/1.png

O cronograma de trabalho pode ser acessado `nesse link <https://gitlab.com/pencillabs/institutogesto/secretariaderecife/-/wikis/Reformula%C3%A7%C3%A3o-do-cronograma-inicial>`_.

.. list-table:: Lista de sistemas avaliados
   :widths: 20 20 20 20
   :header-rows: 1

   * - Sistema
     - Link homologação
     - Acesso homologação
     - Link produção
   * - SEMEWEB - Sistema de Cadastro Escolar
     - www.recife.pe.gov.br/semewebtreinamento
     - Web
     - www.recife.pe.gov.br/matricula
   * - SEGE- Sistema de Gestão Escolar
     - http://bacurauh.recife:8080/sege
     - VPN
     - http://gestaoescolar.recife.pe.gov.br/
   * - SDCL - Sistema Diário de Classe On-line
     - http://setubalh.recife:8080/diariodeclasse
     - VPN
     - http://diariodeclasse.recife.pe.gov.br/
   * - SAAP - Sistema de Acompanhamento de Ações Pedagógicas
     - http://setubalh.recife:8081/saap
     - VPN
     - https://saap.recife.pe.gov.br/
   * - SEGM - Sistema de Alimentação Escolar
     - http://setubalh.recife:8080/merenda
     - VPN
     - https://merenda.recife.pe.gov.br/
   * - SEGP - Sistema de Gestão de Pessoas
     - http://bacurauh.recife:8080/segp
     - VPN
     - Sem ambiente
   * - Matrícula Online
     - https://matriculaonline-dev.recife.pe.gov.br/
     - Web
     - https://matriculaonline.recife.pe.gov.br/
   * - SEINFRA - Planejamento de vagas e capacidade instalada
     - Sem ambiente
     - -
     - http://infrarede.recife.pe.gov.br/
   * - SADR - Sistema de Avaliação diagnóstica da Rede
     - http://cohab.recife/pr/seducacao/sadr/
     - VPN
     - www.recife.pe.gov.br/sadr
   * - GEQT - Sistema de Gestão de Equipamentos
     - http://setubalh.recife:8081/localizacao/
     - VPN
     - http://gestaoequipamentos.recife.pe.gov.br/
   * - Consist - Sistema de Folha de Pagamento
     - sem acesso
     - sem acesso
     - sem acesso
   * - SOFIN - Sistema de Controle Orçamentário e Financeiro
     - sem acesso
     - sem acesso
     - sem acesso



.. toctree::
   :maxdepth: 2
   :caption: Sistemas pedagógicos
   :glob:

   sistemas/pedagogicos/*

.. toctree::
   :maxdepth: 2
   :caption: Sistemas administrativos
   :glob:

   sistemas/administrativos/*

.. toctree::
   :maxdepth: 2
   :caption: Conclusão

   conclusão

